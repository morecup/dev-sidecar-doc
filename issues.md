# 作者在 ow.md 文件中提到的不支持http流量的解决方案 
## 解决方案
1.  请求时加一个 dsscheme 头，携带目标网站的scheme值（http或https）
2.  在nginx反向代理的时候判断一下，代码如下

```adblock
...
set $_scheme $scheme;
if ( $http_dsscheme = 'http' ){
    set $_scheme 'http';
}
...
proxy_pass $_scheme://$_host/$_uri;
```

> 一点小小的建议，希望可以起到作用

# 请问cf-workers现在是不是用不了了

1最近这两天边车链接不上，但是cf后台显示运行正常

2同问，配置了还是
```
DevSidecar Error:
目标网站请求错误：【undefined】 xx.xxx.workers.dev:443, 代理请求超时
目标地址：https://xx.xxx.workers.dev:443/xxxxxx/www.qq.com/
```

3搞个免费的域名绑到cf-worker上，可以解决问题

4去哪里搞的免费域名

5freenom.com, 现在好像不能注册

6请问这个是怎么操作的，能说一下吗哥

7手里有域名的话按照 这篇教程(https://github.com/docmirror/dev-sidecar-doc/blob/main/cf-works.md) 创建完 Worker 后，触发器 > 自定义域，需要事先把域名绑到 CF 上面

8我有看这篇教程，现在是问有了域名后，域名是填在works代码里面还是说在cf的worker路由上

9都要填一样的

# 自己怎么弄一个ssl证书呢？
免费证书随处都可以申请的。
或者也可以用自签名证书，那就需要按照下图去掉这个勾
[![image](https://user-images.githubusercontent.com/1687298/159161257-fd697b37-96f7-45b2-bb72-98b9eb960d1d.png)](https://user-images.githubusercontent.com/1687298/159161257-fd697b37-96f7-45b2-bb72-98b9eb960d1d.png)

# 代理socket 
有没有知道怎么代理Socket连接吗？
我想是不是可以在自己服务器上配置好nginx代理 转发socket就可以实现访问新必应，gpt之类的了。
我看nginx是可以代理socket的

根据https://tizi.blog/352.html 这篇文章推测cloudflare cfworker是支持wss的，这么说应该只是软件缺少代理wss协议的逻辑，不知道有谁能提交这个PR：）