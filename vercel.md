vercel.json 配置分享
在任意 私仓 新建 vercel.json 文件，内容如下
```
{ 
     "rewrites": [ 
         { 
             "source": "/password/:host:slash(/)?", 
             "destination": "https://:host" 
         }, 
         { 
             "source": "/password/:host/:path*:slash(/)?:query(\\?.*)?", 
             "destination": "https://:host/:path:slash:query" 
         }
     ] 
 }
```

部署到 Vercel 并绑定自己的域名，然后配置好 DS 即可。

注意：如果路径里有两个连续的斜杠，则会发生各种奇怪的问题，可以 施法 后参考这篇文章(https://dsy4567.cf/blog/vercel-dev-sidecar-fq/) 魔改 DS 代码